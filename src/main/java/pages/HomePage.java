package pages;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import interfaces.HomePageUI;

public class HomePage extends AbstractPage{

	WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public boolean isDisplayMsgSuccess() {
		return isControlIsDisplay(driver, HomePageUI.SUCCESS_TEXT);
	}
}
