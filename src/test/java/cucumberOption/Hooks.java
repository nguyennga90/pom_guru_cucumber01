package cucumberOption;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import commons.Constants;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

    private static WebDriver driver;
	private static final Logger LOGGER = Logger.getLogger(Hooks.class.getName());

	@Before
	public synchronized static WebDriver openBrowser() {
		if (driver == null) {
			try {
//				ChromeDriverManager.getInstance().version("2.34").setup();
//				ChromeOptions options = new ChromeOptions();
//				options.addArguments("--disable-extensions");
//				
//				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//				capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);

				driver = new FirefoxDriver();
			} catch (UnreachableBrowserException e) {
				driver = new FirefoxDriver();
			} catch (WebDriverException e) {
				driver = new FirefoxDriver();
			} finally {
				Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
			}
			driver.get(Constants.BANK_GURU_URL);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}
		System.out.println("---------START BROWSER---------");
		return driver;
	}

	public static void close() {
		try {
			openBrowser().quit();
			driver = null;
			LOGGER.info("Closing the browser");
		} catch (UnreachableBrowserException e) {
			LOGGER.info("Cannot close browser: unreachable browser");
		}
	}

	private static class BrowserCleanup implements Runnable {
		public void run() {
			close();
		}
	}

	@After
	public static void closeBrowser(WebDriver driver) {
		try {
			if (driver != null) {
				driver.quit();
				System.gc();
				if (driver.toString().toLowerCase().contains("chrome")) {
					String cmd = "taskkill /IM chromedriver.exe /F";
					Process process = Runtime.getRuntime().exec(cmd);
					process.waitFor();
				}
				if (driver.toString().toLowerCase().contains("internetexplorer")) {
					String cmd = "taskkill /IM IEDriverServer.exe /F";
					Process process = Runtime.getRuntime().exec(cmd);
					process.waitFor();
				}
				System.out.println("---------QUIT BROWSER---------");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}