package stepDefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.BalEnquiryPage;
import pages.BalanceEnquiryPage;
import pages.HomePage;
import pages.PageFactoryPage;

public class CheckCurrentAccountBalanceSteps extends AbstractTest{


	WebDriver driver;
	private HomePage homePage;
	private BalanceEnquiryPage balanceEnquiryPage;
	private BalEnquiryPage balEnquiryPage;

	public CheckCurrentAccountBalanceSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}

    @Given("^I open Balance Enquiry page$")
    public void i_open_balance_enquiry_page(){
        balanceEnquiryPage = homePage.openBalanceEnquiryPage(driver);
    }

    @Given("^Input data to all fields required in Balance Enquiry page$")
    public void input_data_to_all_fields_required_in_balance_enquiry_page(){
        balanceEnquiryPage.inputAccNo(CreateAccountSteps.accountID);
    }

    @Given("^Click to Submit button in Balance Enquiry page$")
    public void click_to_submit_button_in_balance_enquiry_page(){
        balEnquiryPage = balanceEnquiryPage.clickSubmitBtn();
    }
    
    @Then("^Verify message displays in Balance Enquiry page$")
    public void verify_message_displays_in_balance_enquiry_page() {
    	verifyTrue(balEnquiryPage.isMsgSuccess(CreateAccountSteps.accountID));
    }


    @Then("^Infomation should be shown in Balance Enquiry page$")
    public void infomation_should_be_shown_in_balance_enquiry_page(DataTable arg1){
        List<List<String>> data = arg1.raw();
        verifyEquals(balEnquiryPage.getBalance(), data.get(1).get(0));
    }

}
