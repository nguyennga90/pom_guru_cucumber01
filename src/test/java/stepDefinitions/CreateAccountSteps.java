package stepDefinitions;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import pages.AccCreateMsgPage;
import pages.HomePage;
import pages.NewAccountPage;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.PageFactoryPage;

public class CreateAccountSteps extends AbstractTest{

	WebDriver driver;
	private HomePage homePage;
	private NewAccountPage newAccountPage;
	private AccCreateMsgPage accCreateMsgPage;
	public static String accountID;

	public CreateAccountSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}
	
	@Given("^I open New Account page$")
	public void iOpenNewAccountPage() {
	    newAccountPage = homePage.openNewAccountPage(driver);	    
	}

	@Given("^Input data to all fields required in New Account page$")
	public void inputDataToAllFieldsRequiredInNewAccountPage(DataTable arg1) {
		List<Map<String,String>> data = arg1.asMaps(String.class, String.class);
		newAccountPage.inputCustomerId(CreateNewCustomerSteps.customerID);
		newAccountPage.chooseAccountType(data.get(0).get("Account Type"));
		newAccountPage.inputDeposit(data.get(0).get("Initial deposit"));
	}

	@Given("^Click to Submit button in New Account page$")
	public void clickToSubmitButtonInNewAccountPage() {
	    accCreateMsgPage = newAccountPage.clickSubmitButton();	    
	}

	@Then("^Verify message displays with content \"(.*?)\"$")
	public void verifyMessageDisplaysWithContent(String arg1) {
		verifyTrue(accCreateMsgPage.isRegisAccountSuccess());	    
	}

	@Then("^Account infomation should be shown$")
	public void accountInfomationShouldBeShown(DataTable arg1) {
		List<List<String>> data = arg1.raw();
		verifyEquals(accCreateMsgPage.getCurrentAmount(), data.get(1).get(0));
	}

	@Then("^Get AccountID for transfering money$")
	public void getAccountIDForTransferingMoney() {
	    accountID = accCreateMsgPage.getAccountID();	    
	}
}
