package stepDefinitions;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumberOption.Hooks;
import pages.EditCustomerPage;
import pages.EditDetailCustomerPage;
import pages.HomePage;
import pages.PageFactoryPage;

public class EditCustomerSteps {

	WebDriver driver;
	private HomePage homePage;
	private EditCustomerPage editCustomerPage;
	private EditDetailCustomerPage editDetailCustomerPage;

	public EditCustomerSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}

	@Given("^I open Edit Customer page$")
	public void iOpenEditCustomerPage() {
		editCustomerPage = homePage.openEditCustomerPage(driver);
	}

	@When("^Input CustomerID$")
	public void inputCustomerID() {
		editCustomerPage.inputCustomerID(CreateNewCustomerSteps.customerID);
	}

	@When("^Click to Submit button EditCustomer$")
	public void clickToSubmitButtonEditCustomer() {
		editDetailCustomerPage = editCustomerPage.clickSubmitBtn();
	}

}
