package stepDefinitions;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.DeleteCustomerPage;
import pages.HomePage;
import pages.PageFactoryPage;

public class DeleteCustomerSteps extends AbstractTest {

	WebDriver driver;
	private HomePage homePage;
	private DeleteCustomerPage deleteCustomerPage;

	public DeleteCustomerSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}

	@Given("^I open Delete Customer page$")
	public void i_open_delete_customer_page() {
		deleteCustomerPage = homePage.openDeleteCustomerPage(driver);
	}

	@Given("^Input data to all fields required in Delete Customer page$")
	public void input_data_to_all_fields_required_in_delete_customer_page() {
		deleteCustomerPage.inputCutomerId(CreateNewCustomerSteps.customerID);
	}

	@Given("^Click to Submit button in Delete Customer page$")
	public void click_to_submit_button_in_delete_customer_page() {
		deleteCustomerPage.clickSubmitBtn();
	}

	@Then("^I quit browser$")
	public void iQuitBrowser() {
		Hooks.closeBrowser(driver);
	}
}
