package stepDefinitions;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import pages.DepositInfoPage;
import pages.DepositPage;

import commons.AbstractTest;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.HomePage;
import pages.PageFactoryPage;

public class TransferCurrentAccountSteps extends AbstractTest{

	WebDriver driver;
	private HomePage homePage;
	private DepositPage depositPage;
	private DepositInfoPage depositInfoPage;

	public TransferCurrentAccountSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}

	@Given("^I open Deposit page$")
	public void iOpenDepositPage(){
		depositPage = homePage.openDepositPage(driver);		
	}

	@Given("^Input data to all fields required in Deposit page$")
	public void inputDataToAllFieldsRequiredInDepositPage(DataTable arg1){
		List<Map<String,String>> data = arg1.asMaps(String.class, String.class);
		depositPage.inputAccId(CreateAccountSteps.accountID);
		depositPage.inputAmount(data.get(0).get("Amount"));
		depositPage.inputDescription(data.get(0).get("Description"));
	}

	@Given("^Click to Submit button in Deposit page$")
	public void clickToSubmitButtonInDepositPage(){
		depositInfoPage = depositPage.clickSubmitBtn();		
	}

	@Then("^Verify message displays in DepositInf page$")
	public void verifyMessageDisplaysInDepositInfPage(){
		verifyTrue(depositInfoPage.isMsgDeposit(CreateAccountSteps.accountID));		
	}

	@Then("^Infomation should be shown in DepositInf page$")
	public void infomationShouldBeShownInDepositInfPage(DataTable arg1){
		List<Map<String,String>> data = arg1.asMaps(String.class, String.class);
		verifyEquals(depositInfoPage.getCurrentBalance(), data.get(0).get("Current Amount"));
	}

}
