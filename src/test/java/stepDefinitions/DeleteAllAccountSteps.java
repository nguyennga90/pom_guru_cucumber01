package stepDefinitions;

import org.openqa.selenium.WebDriver;

import pages.DeleteAccountPage;

import commons.AbstractTest;
import cucumber.api.java.en.Given;
import cucumberOption.Hooks;
import pages.HomePage;
import pages.PageFactoryPage;

public class DeleteAllAccountSteps extends AbstractTest {



	WebDriver driver;
	private HomePage homePage;
	private DeleteAccountPage deleteAccountPage;

	public DeleteAllAccountSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}

    @Given("^I open Delete Account page1$")
    public void i_open_delete_account_page1(){
        deleteAccountPage = homePage.openDeleteAccountPage(driver);
    }

    @Given("^Input data to all fields required in Delete Account page1$")
    public void input_data_to_all_fields_required_in_delete_account_page1(){
        deleteAccountPage.inputAccNo(CreateAccountSteps.accountID);
    }

    @Given("^Click to Submit button in Delete Account page1$")
    public void click_to_submit_button_in_delete_account_page1(){
        homePage = deleteAccountPage.clickSubmitBtn();
    }

    @Given("^I open Delete Account page2$")
    public void i_open_delete_account_page2(){
        deleteAccountPage = homePage.openDeleteAccountPage(driver);
    }

    @Given("^Input data to all fields required in Delete Account page2$")
    public void input_data_to_all_fields_required_in_delete_account_page2(){
        deleteAccountPage.inputAccNo(TransferOtherAccountSteps.payeeAccountID);
    }

    @Given("^Click to Submit button in Delete Account page2$")
    public void click_to_submit_button_in_delete_account_page2(){
        homePage = deleteAccountPage.clickSubmitBtn();
    }

}
