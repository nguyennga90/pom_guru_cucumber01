package stepDefinitions;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.HomePage;
import pages.LoginPage;
import pages.PageFactoryPage;
import pages.RegisterPage;

public class LoginSteps extends AbstractTest {

	WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private RegisterPage registerPage;
	String mail = "ngantt" + randomNumber() + "@gmail.com";
	String loginURL, userId, password;

	public LoginSteps() {
		driver = Hooks.openBrowser();
		loginPage = PageFactoryPage.getLoginPage(driver);
	}

	@Given("^I create new account guru$")
	public void i_create_new_account_guru() {
		loginURL = loginPage.getCurrentUrl();
		registerPage = loginPage.clickHereLink();

		registerPage.regisNewMail(mail);
		registerPage.clickSubmitBtn();
		userId = registerPage.getUserInfo();
		password = registerPage.getPasswordInfo();
	}

	@Given("^I back to login page$")
    public void i_back_to_login_page() {
		loginPage = registerPage.returnLoginPage(loginURL);
	}

	@Given("^I put username and password$")
	public void iPutUsernameAndPassword() {
		loginPage.enterUserId(userId);
		loginPage.enterPassword(password);

	}

	@Given("^Click to button Login$")
	public void clickToButtonLogin() {
		homePage = loginPage.clickSubmitBtn();
	}

	@Then("^Verify HomePage displayed$")
	public void verifyHomePageDisplayed() {
		verifyTrue(homePage.isDisplayMsgSuccess());
	}

}
