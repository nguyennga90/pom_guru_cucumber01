package stepDefinitions;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import pages.WithdrawalInfoPage;
import pages.WithdrawalPage;

import commons.AbstractTest;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.HomePage;
import pages.PageFactoryPage;

public class WithdrawMoneyFromCurrentAccountSteps extends AbstractTest{

	WebDriver driver;
	private HomePage homePage;
	private WithdrawalPage withdrawalPage;
	private WithdrawalInfoPage withdrawalInfoPage;

	public WithdrawMoneyFromCurrentAccountSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}

	@Given("^I open Withdrawal page$")
	public void iOpenWithdrawalPage(){
		withdrawalPage = homePage.openWithdrawalPage(driver);		
	}

	@Given("^Input data to all fields required in Withdrawal page$")
	public void inputDataToAllFieldsRequiredInWithdrawalPage(DataTable arg1){
		List<Map<String, String>> data = arg1.asMaps(String.class, String.class);
		withdrawalPage.inputAccId(CreateAccountSteps.accountID);
		withdrawalPage.inputAmountDebit(data.get(0).get("Amount"));
		withdrawalPage.inputDescription(data.get(0).get("Description"));
	}

	@Given("^Click to Submit button in Withdrawal page$")
	public void clickToSubmitButtonInWithdrawalPage(){
		withdrawalInfoPage = withdrawalPage.clickSubmitBtn();		
	}

	@Then("^Verify message displays in WithdrawalInf page$")
	public void verifyMessageDisplaysInWithdrawalInfPage(){
		verifyTrue(withdrawalInfoPage.isMsgWithdrawal(CreateAccountSteps.accountID));		
	}

	@Then("^Infomation should be shown in WithdrawalInf page$")
	public void infomationShouldBeShownInWithdrawalInfPage(DataTable arg1){
		List<Map<String, String>> data = arg1.asMaps(String.class, String.class);
		verifyEquals(withdrawalInfoPage.getCurrentBalance(), data.get(0).get("Current Amount"));
	}

}
