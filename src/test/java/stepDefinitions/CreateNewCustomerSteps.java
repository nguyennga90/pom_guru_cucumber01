package stepDefinitions;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.CustomerRegMsgPage;
import pages.HomePage;
import pages.NewCustomerPage;
import pages.PageFactoryPage;

public class CreateNewCustomerSteps extends AbstractTest {

	WebDriver driver;
	private HomePage homePage;
	private NewCustomerPage newCustomerPage;
	private CustomerRegMsgPage customerRegMsgPage;
	String strMail = "automation" + randomNumber() + "@gmail.com";
	public static String customerID;

	public CreateNewCustomerSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}

	@Given("^I open New Customer page$")
	public void iOpenNewCustomerPage() {
		newCustomerPage = homePage.openNewCustomerPage(driver);
	}

	@Given("^Input data to all fields required$")
	public void inputDataToAllFieldsRequired(DataTable arg1) {
		List<Map<String,String>> data = arg1.asMaps(String.class, String.class);
		newCustomerPage.inputCustomerName(data.get(0).get("CustomerName"));
		newCustomerPage.chooseGenderMale();
		newCustomerPage.inputBirthday(data.get(0).get("DateOfBirth"));
		newCustomerPage.inputAddress(data.get(0).get("Address"));
		newCustomerPage.inputCity(data.get(0).get("City"));
		newCustomerPage.inputState(data.get(0).get("State"));
		newCustomerPage.inputPin(data.get(0).get("PIN"));
		newCustomerPage.inputPhoneNo(data.get(0).get("Phone"));
		newCustomerPage.inputEmail(strMail);
		newCustomerPage.inputPass(data.get(0).get("Password"));
	}

	@Given("^Click to Submit button NewCustomer$")
	public void clickToSubmitButtonNewCustomer() {
		customerRegMsgPage = newCustomerPage.clickSubmitBtn();
	}

	@Then("^Verify Customer created successfully with message \"(.*?)\"$")
	public void verifyCustomerCreatedSuccessfullyWithMessage(String arg1) {
		verifyTrue(customerRegMsgPage.isRegisCustomerSuccess());
	}

	@Then("^Customer infomation should be shown$")
	public void customerInfomationShouldBeShown(DataTable arg1) {
		List<Map<String,String>> data = arg1.asMaps(String.class, String.class);
		verifyEquals(customerRegMsgPage.getCustomerName(), data.get(0).get("CustomerName"));
		verifyEquals(customerRegMsgPage.getCity(), data.get(0).get("City"));
		verifyEquals(customerRegMsgPage.getAddress(), data.get(0).get("Address"));
		verifyEquals(customerRegMsgPage.getPhoneNo(), data.get(0).get("Phone"));
		verifyEquals(customerRegMsgPage.getEmail(), strMail);
	}

	@Then("^Get CustomerID for edit customer function$")
	public void getCustomerIDForEditCustomerFunction() {
		customerID = customerRegMsgPage.getCustomerID();
	}

}
