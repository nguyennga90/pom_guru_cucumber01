package stepDefinitions;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import pages.FundTransPage;
import pages.FundTransferPage;

import commons.AbstractTest;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.AccCreateMsgPage;
import pages.HomePage;
import pages.NewAccountPage;
import pages.PageFactoryPage;

public class TransferOtherAccountSteps extends AbstractTest{

	WebDriver driver;
	private HomePage homePage;
	private NewAccountPage newAccountPage;
	private AccCreateMsgPage accCreateMsgPage;
	private FundTransferPage fundTransferPage;
	private FundTransPage fundTransPage;
	public static String payeeAccountID;

	public TransferOtherAccountSteps() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryPage.getHomePage(driver);
	}

	@Given("^I open New Other Account page$")
	public void iOpenNewOtherAccountPage(){
		newAccountPage = homePage.openNewAccountPage(driver);		
	}

	@Given("^Input data to all fields required in New Other Account page$")
	public void inputDataToAllFieldsRequiredInNewOtherAccountPage(DataTable arg1){
		List<Map<String, String>> data = arg1.asMaps(String.class, String.class);
		newAccountPage.inputCustomerId(CreateNewCustomerSteps.customerID);
		newAccountPage.chooseAccountType(data.get(0).get("Account Type"));
		newAccountPage.inputDeposit(data.get(0).get("Initial deposit"));
	}

	@Given("^Click to Submit button in New Other Account page$")
	public void clickToSubmitButtonInNewOtherAccountPage(){
		accCreateMsgPage = newAccountPage.clickSubmitButton();		
	}

	@Given("^Get Other AccountID for transfering money$")
	public void getOtherAccountIDForTransferingMoney(){
		payeeAccountID = accCreateMsgPage.getAccountID();		
	}

	@Given("^I open Fund Transfer page$")
	public void iOpenFundTransferPage(){
		fundTransferPage = accCreateMsgPage.openFundTransferPage(driver);		
	}

	@Given("^Input data to all fields required in Fund Transfer page$")
	public void inputDataToAllFieldsRequiredInFundTransferPage(DataTable arg1){
		List<Map<String, String>> data = arg1.asMaps(String.class, String.class);
		fundTransferPage.inputAccPayer(CreateAccountSteps.accountID);
		fundTransferPage.inputAccPayee(payeeAccountID);
		fundTransferPage.inputAmountTransfer(data.get(0).get("Amount"));
		fundTransferPage.inputDescription(data.get(0).get("Description"));
	}

	@Given("^Click to Submit button in Fund Transfer page$")
	public void clickToSubmitButtonInFundTransferPage(){
		fundTransPage = fundTransferPage.clickSubmitBtn();		
	}

	@Then("^Infomation should be shown in Fund Transfer page$")
	public void infomationShouldBeShownInFundTransferPage(DataTable arg1){
		List<Map<String, String>> data = arg1.asMaps(String.class, String.class);
		verifyEquals(fundTransPage.getAmount(), data.get(0).get("Amount"));
	}

}
