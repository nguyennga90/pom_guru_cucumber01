@payment
Feature: Payment feature
  I want to create new customer

  @login
  Scenario: Login
    Given I create new account guru
    And I back to login page
    And I put username and password
    And Click to button Login
    Then Verify HomePage displayed

  @newCustomer
  Scenario: Create new Customer and get NewCustomerID
    Given I open New Customer page
    And Input data to all fields required
      | CustomerName | Gender | DateOfBirth | Address | City    | State   | PIN    | Phone     | Email      | Password |
      | Dam Dao      | m      | 16/10/1989  | Da nang | Da nang | Da nang | 456789 | 123456789 | automation |   123456 |
    And Click to Submit button NewCustomer
    Then Verify Customer created successfully with message "Customer Registered Successfully!!!"
    And Customer infomation should be shown
      | CustomerName | City    | Address | Phone     | Email    |
      | Dam Dao      | Da nang | Da nang | 123456789 | autotest |
    And Get CustomerID for edit customer function

  @editCustomer
  Scenario: Edit Customer
    Given I open Edit Customer page
    When Input CustomerID
    And Click to Submit button EditCustomer

  @newAccount
  Scenario: Create new Account
    Given I open New Account page
    And Input data to all fields required in New Account page
      | Account Type | Initial deposit |
      | Current      |           50000 |
    And Click to Submit button in New Account page
    Then Verify message displays with content "Account Generated Successfully!!!"
    And Account infomation should be shown
      | Initial deposit |
      |           50000 |
    And Get AccountID for transfering money

  @transferCurrentAccount
  Scenario: Transfer money into a current account and check account balance equal 55,000
    Given I open Deposit page
    And Input data to all fields required in Deposit page
      | Amount | Description |
      |   5000 | Deposit     |
    And Click to Submit button in Deposit page
    Then Verify message displays in DepositInf page
    And Infomation should be shown in DepositInf page
      | Current Amount |
      |          55000 |

  @withdrawMoneyFromCurrentAccount
  Scenario: Withdraw money from current account and check account balance equal 40,000
    Given I open Withdrawal page
    And Input data to all fields required in Withdrawal page
      | Amount | Description |
      |  15000 | Withdraw    |
    And Click to Submit button in Withdrawal page
    Then Verify message displays in WithdrawalInf page
    And Infomation should be shown in WithdrawalInf page
      | Current Amount |
      |          40000 |

  @transferMoneyToAnotherAccount
  Scenario: Transfer the money into another account and check amount equal 10,000
    Given I open New Other Account page
    And Input data to all fields required in New Other Account page
      | Account Type | Initial deposit |
      | Current      |           50000 |
    And Click to Submit button in New Other Account page
    And Get Other AccountID for transfering money
    And I open Fund Transfer page
    And Input data to all fields required in Fund Transfer page
      | Amount | Description |
      |  10000 | Transfer    |
    And Click to Submit button in Fund Transfer page
    Then Infomation should be shown in Fund Transfer page
      | Amount |
      |  10000 |

  @checkCurrentAccountBalance
  Scenario: Check current account balance equal 30,000
    Given I open Balance Enquiry page
    And Input data to all fields required in Balance Enquiry page
    And Click to Submit button in Balance Enquiry page
    Then Verify message displays in Balance Enquiry page
    And Infomation should be shown in Balance Enquiry page
      | Balance |
      |   30000 |

  @deleteAllAccount
  Scenario: Delete all account of this customer account and check deleted successfully
    Given I open Delete Account page1
    And Input data to all fields required in Delete Account page1
    And Click to Submit button in Delete Account page1
    And I open Delete Account page2
    And Input data to all fields required in Delete Account page2
    And Click to Submit button in Delete Account page2
    
  @deleteExistCustomer
  Scenario: Delete exist customer account and check deleted successfully
    Given I open Delete Customer page
    And Input data to all fields required in Delete Customer page
    And Click to Submit button in Delete Customer page
    Then I quit browser
